from setuptools import setup, find_packages

setup(
		name = 'HtpasswdBackend',
		version = "0.1.4",
		author = "Michiel van Baak",
		author_email = "michiel@vanbaak.info",
		description = "reviewboard htpasswd auth backend",
		#long_description = description,
		packages = find_packages(exclude=['ez_setup']),
		include_package_data = True,
		zip_safe = False,
		entry_points={
			'reviewboard.auth_backends': [
				'HtpasswdBackend = HtpasswdBackend:HtpasswdBackend',
			],
		}
)
