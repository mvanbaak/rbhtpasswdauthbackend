import logging
from django.conf import settings
from django.contrib.auth.models import User
import reviewboard

class HtpasswdBackend(reviewboard.accounts.backends.AuthBackend):
	name = "HtpasswdBackend"
	#XXX: needs to be implemented settings_form = HtpasswdBackendSettingsForm
	supports_change_name = True
	supports_change_email = True

	# From: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/325204
	def _apache_md5crypt(self, password, salt, magic='$apr1$'):
		"""
		Calculates the Apache-style MD5 hash of a password
		"""
		# /* The password first, since that is what is most unknown */ /* Then our magic string */ /* Then the raw salt */
		import md5
		m = md5.new()
		m.update(password + magic + salt)

		# /* Then just as many characters of the MD5(pw,salt,pw) */
		mixin = md5.md5(password + salt + password).digest()
		for i in range(0, len(password)):
			m.update(mixin[i % 16])

		# /* Then something really weird... */
		# Also really broken, as far as I can tell.  -m
		i = len(password)
		while i:
			if i & 1:
				m.update('\x00')
			else:
				m.update(password[0])
			i >>= 1

		final = m.digest()

		# /* and now, just to make sure things don't run too fast */
		for i in range(1000):
			m2 = md5.md5()
			if i & 1:
				m2.update(password)
			else:
				m2.update(final)

			if i % 3:
				m2.update(salt)

			if i % 7:
				m2.update(password)

			if i & 1:
				m2.update(final)
			else:
				m2.update(password)

			final = m2.digest()

		# This is the bit that uses to64() in the original code.

		itoa64 = './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'

		rearranged = ''
		for a, b, c in ((0, 6, 12), (1, 7, 13), (2, 8, 14), (3, 9, 15), (4, 10, 5)):
			v = ord(final[a]) << 16 | ord(final[b]) << 8 | ord(final[c])
			for i in range(4):
				rearranged += itoa64[v & 0x3f]; v >>= 6

		v = ord(final[11])
		for i in range(2):
			rearranged += itoa64[v & 0x3f]; v >>= 6

		return magic + salt + '$' + rearranged

	def _check_entry_password(self, username, password, entry_password):
		if entry_password.startswith('$apr1$'):
			salt = entry_password[6:].split('$')[0][:8]
			expected = self._apache_md5crypt(password, salt)
		elif entry_password.startswith('{SHA}'):
			import sha
			expected = '{SHA}' + sha.new(password).digest().encode('base64').strip()
		else:
			import crypt
			expected = crypt.crypt(password, entry_password)

		return entry_password == expected

	def _parse_htpasswd(self, password_file, stop_username=None):
		"""
		Returns a dictionary of usernames and hashed password entries.  If
		stop_username is given, parsing will be finished as soon as that
		username is encountered.
		"""
		fh = open(password_file, 'rb')
		try:
			entries = {}
			for line in fh.readlines():
				line = line.strip()
				if not line or line.startswith('#'):
					continue
				if ':' not in line:
					raise ValueError("Bad line (no :): %r" % line)
				username, entry_password = line.split(':', 1)
				entries[username] = entry_password
				if username == stop_username:
					break
			return entries
		finally:
			fh.close()

	def _check_password(self, username, password, password_file):
		entries = self._parse_htpasswd(password_file, username)
		if not entries.has_key(username):
			return False

		return self._check_entry_password(username, password, entries[username])

	def authenticate(self, username, password):
		#password_file = settings.HTPASSWD_FILENAME
		#password_file = password_file.strip()
		password_file = '/usr/local/etc/facturun.htpasswd'
		username = username.strip()
		password = password.strip()

		login_valid = self._check_password(username, password, password_file)

		if not login_valid:
			return None

		user = self.get_or_create_user(username, None)
		return user

	def get_or_create_user(self, username, request):
		import nis

		username = username.strip()

		try:
			user = User.objects.get(username=username)
		except User.DoesNotExist:
			try:
				first_name = username
				last_name = ''
				email = ''

				user = User(username=username,
					password='',
					first_name=first_name,
					last_name=last_name,
					email=email)
				user.is_staff = False
				user.is_superuser = False
				user.set_unusable_password()
				user.save()
				return user
			except nis.error:
				pass
		return user
